#!/usr/bin/make -f

# DH_VERBOSE := 1
export LC_ALL=C.UTF-8

include /usr/share/dpkg/default.mk

# for hardening you might like to uncomment this:
export DEB_BUILD_MAINT_OPTIONS=hardening=+all
export DEB_CFLAGS_MAINT_APPEND=-DSIMDE_ENABLE_OPENMP -fopenmp-simd
export CFLAGS=$(shell dpkg-buildflags --get CFLAGS)
export CPPFLAGS=$(shell dpkg-buildflags --get CPPFLAGS)
export LDFLAGS=$(shell dpkg-buildflags --get LDFLAGS)

prefix=$(CURDIR)/debian/$(DEB_SOURCE)/usr
libexecdir=$(prefix)/lib/$(DEB_SOURCE)
BINARIES=kbm2 wtdbg2 wtdbg-cns wtpoa-cns pgzf
AMD64_FLAGS=avx2 avx sse4.1 ssse3 sse3 sse2
I386_FLAGS=sse2 sse mmx

%:
	dh $@

override_dh_auto_build:
	sed -i.dh_clean \
		-e '1s/\/usr\/bin\/env perl/\/usr\/bin\/perl/' \
		-e 's/memeory/memory/' \
		-e 's/automaticly/automatically/' \
		-e 's/ouput/output/' \
		-e 's/vaule/value/' \
		-e 's/annonymous/anonymous/' \
		scripts/* *.c *.h *.md
ifeq (amd64,$(DEB_HOST_ARCH))
	mkdir -p $(prefix)
	mkdir -p $(libexecdir)
	for SIMD in $(AMD64_FLAGS); do \
		export CFLAGS="$(CFLAGS) -m$${SIMD}" ; \
		make clean ; \
		dh_auto_build -- SFX=-$${SIMD} ; \
	done
else ifeq (i386,$(DEB_HOST_ARCH))
	mkdir -p $(prefix)
	mkdir -p $(libexecdir)
	for SIMD in $(I386_FLAGS); do \
		export CFLAGS="$(CXXFLAGS) -m$${SIMD}" ; \
		make clean ; \
		dh_auto_build -- SFX=-$${SIMD} ; \
	done
	dh_auto_build -- SFX=-plain
else
	dh_auto_build
endif

override_dh_auto_install:
ifeq (amd64,$(DEB_HOST_ARCH))
	dh_install -p $(DEB_SOURCE) debian/bin/simd-dispatch /usr/lib/$(DEB_SOURCE)/
	for SIMD in $(AMD64_FLAGS); do \
		export CFLAGS="$(CFLAGS) -m$${SIMD}" ; \
		dh_auto_build -- SFX=-$${SIMD} BIN=$(libexecdir) install ; \
	done
	mkdir -p $(prefix)/bin
	cd $(prefix)/bin \
		&& for prog in $(BINARIES); do \
		ln -s ../lib/$(DEB_SOURCE)/simd-dispatch $${prog} ; done
else ifeq (i386,$(DEB_HOST_ARCH))
	dh_install -p $(DEB_SOURCE) debian/bin/simd-dispatch /usr/lib/$(DEB_SOURCE)/
	for SIMD in $(I386_FLAGS); do \
		export CFLAGS="$(CFLAGS) -m$${SIMD}" ; \
		dh_auto_build -- SFX=-$${SIMD} BIN=$(libexecdir) install ; \
	done
	export CLFAGS="$(CFLAGS)" ; dh_auto_build -- SFX=-plain BIN=$(libexecdir) \
		install
	mkdir -p $(prefix)/bin
	cd $(prefix)/bin \
		&& for prog in $(BINARIES); do \
		ln -s ../lib/$(DEB_SOURCE)/simd-dispatch $${prog} ; done
else
	make BIN=debian/$(DEB_SOURCE)/usr/bin install
endif

execute_after_dh_auto_clean:
ifeq (amd64,$(DEB_HOST_ARCH))
	$(RM) $(foreach binary,$(BINARIES), \
		$(foreach flag,$(AMD64_FLAGS), \
			$(binary)-$(flag) \
		) \
	) # Cleanup additional binaries built for amd64 sub architectures.
else ifeq (i386,$(DEB_HOST_ARCH))
	$(RM) $(foreach binary,$(BINARIES), \
		$(foreach flag,$(I386_FLAGS), \
			$(binary)-$(flag) \
		) \
	) # Cleanup additional binaries built for i386 sub architectures.
endif
	: $(foreach f,$(wildcard *.dh_clean scripts/*.dh_clean), \
		&& mv -v $(f) $(f:.dh_clean=) \
	) # Cleanup the wealth of files modified by sed for various typos.

# Avoid introduction of backup files from the sed stream in binary packages.
execute_after_dh_install:
	find debian/ -name '*.dh_clean' -delete
